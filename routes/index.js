require('../app')

app.get('/', function(req, res){
  res.sendFile('/index.html')
})

module.exports = app;
