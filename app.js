express = require('express');
app = express();
server = require('http').createServer(app);
io = require('socket.io')(server);
bodyParser = require('body-parser').urlencoded({extended: false});
require('./db');

require('./models/chats')

app.use(bodyParser);
app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/public'));

var indexPage =  require('./routes/index');
var postMessage = require('./routes/_post_msg')

app.get('/', indexPage);
app.post('/post', postMessage);

io.on('connection', function(client){
  client.on('data', function(){
    chat_models.find({}, function(err, result){
      if (err) console.log(err);
      var data = JSON.stringify(result)
      client.emit('nihData', data)
    });
  })
  client.on('join', function(data){
    client.broadcast.emit('welcome', '<sub><b>'+data+'</b> Telah masuk ke room<br></sub>');
    // client.emit('welcome', '<sub><b>'+data+'</b> Telah masuk ke room<br></sub>');
  })
})

server.listen(9070);
console.log('App running at port 9070');